CXX=clang++
CPPFLAGS+=`pkg-config --cflags ImageMagick`
CXXFLAGS=-Wall -Wextra -pedantic -std=c++1y -g
LDLIBS=-lMagick++-6.Q16 -ltbb -pthread
SRC=src/main.cc\
    src/sphere.cc\
    src/camera.cc\
    src/entity.cc\
    src/vectors.cc\
    src/directional_light.cc\
    src/point.cc\
    src/color.cc
OBJS=$(SRC:.cc=.o)

all: ${OBJS}
	${CXX} ${CXXFLAGS} ${LDFLAGS} ${LDLIBS} ${INCLUDES} ${OBJS} -o raytracer

clean:
	rm -f ${OBJS}
	rm -f raytracer
