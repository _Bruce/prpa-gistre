#include "sphere.hh"

Point Sphere::get_center()
{
    return this->center;
}

float Sphere::get_diffco()
{
    return this->diff_co;
}

Color Sphere::get_color()
{
    return this->color;
}

Sphere::~Sphere()
{
}

Sphere::Sphere(Point center, float radius, Color color, float diff_co) :
Entity(center), color(color)
{
    this->radius = radius;
    this->diff_co = diff_co;
}

/**
 * Computes the intersection between a ray and a sphere.
 * This solves the following equation :
 * r^2*t^2 + 2r(O-C)t + (abs(O-C))^2 - R^2 = 0
 * Where
 *  * C the center of the sphere (coordinates)
 *  * R is the radius of the sphere
 *  * r the direction of the ray (coordinates)
 *  * O the origin of the ray (coordinates)
 *
 * Products between coordinates are dot products.
 *
 * This is a simple second degree equation (where b = 2r(O-C), a = r^2 and c =
 * (abs(O-C))^2 - R^2), we just have to solve it in order
 * to find its solutions and create a point according to the results :
 * * Two solutions : the closest one is our point
 * * One solution : the only point that emerges from the intersection
 * * Zero solutions : this means that the given point is a null pixel
 */

Point Sphere::intersect(Point ray, Point ray_dir)
{
    // Pre-computation
    float rsquared = Vector::dot_product(ray_dir.to_tuple(), ray_dir.to_tuple()); // r^2
    std::tuple<float, float, float> rtimestwo = Vector::scale(2, ray_dir.to_tuple()); // 2r
    std::tuple<float, float, float> minusc = Vector::scale(-1, this->center.to_tuple()); // -C

    std::tuple<float, float, float> sub = Vector::add_vectors(ray.to_tuple(), minusc); // O - C

    // Delta computation
    float a = rsquared;
    float b = Vector::dot_product(rtimestwo, sub);
    float c = Vector::dot_product(sub, sub) - (this->radius * this->radius);
    float delta = (b * b) - 4 * a * c;

    if (delta < 0)
    {
        return std::make_tuple<float, float, float>(-1, -1, -1);
    }
    else
    {
        float x;
        if (delta == 0)
        {
            x = -b / 2 * a;
        }
        else
        {
            float x1 = (-b + sqrt(delta)) / 2*a;
            float x2 = (-b - sqrt(delta)) / 2*a;
            x = (x1 > x2) ? x2 : x1;
        }
        std::tuple<float, float, float> point =
        Vector::add_vectors(ray.to_tuple(),
        Vector::scale(x, ray_dir.to_tuple()));
        return point;
    }
}

