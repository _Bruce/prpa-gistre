#include "camera.hh"
#include "vectors.hh"

Camera::Camera(Point u1, Point v1, Point center) : Entity(center), u(u1), v(v1)
{}

Camera::~Camera()
{
}

Point Camera::get_center()
{
    return this->center;
}
