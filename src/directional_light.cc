#include "directional_light.hh"

Dlight::Dlight(std::tuple<float, float, float> dir, Color color) : dir(dir), color(color)
{}

Dlight::~Dlight()
{}

Dlight::Dlight() : color(Color(0, 0, 0))
{}

std::tuple<float, float, float> Dlight::get_dir()
{
    return this->dir;
}

Color Dlight::get_color()
{
    return this->color;
}
