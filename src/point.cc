#include "point.hh"

Point::Point(float x, float y, float z) : x(x), y(y), z(z)
{}

Point::Point(std::tuple<float, float, float> coor) : x(std::get<0>(coor)),
                                                     y(std::get<1>(coor)),
                                                     z(std::get<2>(coor))
{}

Point::~Point()
{}

std::tuple<float, float, float> Point::to_tuple() const
{
    return std::make_tuple(this->x, this->y, this->z);
}

float Point::get_x()
{
    return this->x;
}

float Point::get_y()
{
    return this->y;
}

float Point::get_z()
{
    return this->z;
}
