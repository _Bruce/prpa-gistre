#pragma once
#include "entity.hh"
#include "vectors.hh"
#include "color.hh"
#include <cmath>

class Sphere : public Entity
{
public:
    Sphere(Point center, 
           float radius,
           Color color,
           float diff_co);
    float get_diffco();
    Color get_color();
    Color color_compute();
    Point get_center() override;
    Point intersect(Point ray, Point ray_dir);
    ~Sphere();
protected:
    int radius;
    // Color of the sphere
    Color color;
    // Diffusion coefficient, 0 <= diff_co <= 1
    float diff_co;
    // Reflexion coefficient (mirror effect), 0 <= refl_co <= 1
    float refl_co;
};
