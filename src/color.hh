#pragma once
#include <tuple>

class Color
{
public:
    Color(float r, float g, float b);
    Color();
    Color operator*(Color rhs);
    Color operator*(float rhs);
    Color operator+(Color rhs);
    std::tuple<float, float, float> to_tuple();
    float get_r();
    float get_g();
    float get_b();
    ~Color();
private:
    float r;
    float g;
    float b;
};
