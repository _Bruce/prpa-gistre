#include "entity.hh"
#include "camera.hh"
#include "sphere.hh"
#include "Magick++.h"
#include "vectors.hh"
#include "directional_light.hh"
#include "point.hh"
#include "timer.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <thread>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <tbb/mutex.h>
#include <mutex>

/**
 * Computes a needed constant for diffuse lighting colorization.
 * The color of a lighted (or not point is) : color of the light * color of the
 * object * ld
 * Where ld = diff * L * N
 *  * diff is the diffusion coefficient (how the object diffuses the recieved
 *  light)
 *  * L the light direction vector
 *  * N the surface normal of the considered point
 */

float dir_light_const_compute(std::tuple<float, float, float> dlight_dir,
                              std::tuple<float, float, float> ptr_coor,
                              float diff_coef)
{

    // cosine of the angle between the surface normal and the
    // directional light vector
    auto costheta = Vector::dot_product(dlight_dir, ptr_coor);
    costheta = (costheta < 0) ? 0 : costheta; // Clamping the angle
    return diff_coef * costheta;
}


/*
 * Computes the final color of the considerated point of a sphere.
 */
Color color_sphere_compute(Sphere s, Dlight dl, Point p, Color alight)
{
    auto ambient = (alight * s.get_color()) * 0.1f;

    auto ld = dir_light_const_compute(dl.get_dir(),
            p.to_tuple(), s.get_diffco());

    // Color computation

    Color addcolors = (dl.get_color() * s.get_color()) * ld;

    return (ambient + addcolors);
}

/*
 * Initializes the scene with an input file
 */

void conf_read(char* file_path, int& height, int& width,
               std::vector<Sphere::Sphere>& s,
               std::vector<Dlight::Dlight>& dir_light,
               Color& alight)
{
    std::string line;
    std::ifstream file(file_path);
    while (std::getline(file, line))
    {
        // Split the ligne into an array
        std::istringstream string_stream(line);
        std::istream_iterator<std::string> begin(string_stream), end;
        std::vector<std::string> string_split(begin, end);

        auto type = string_split[0];
        if (type == "screen")
        {
            width = std::stoi(string_split[1]);
            height = std::stoi(string_split[2]);
        }
        if (type == "sphere")
        {
            Point center = Point(std::atof(string_split[2].c_str()),
                                 std::atof(string_split[3].c_str()),
                                 std::atof(string_split[4].c_str()));
            float radius = std::atof(string_split[1].c_str());
            float r = std::atof(string_split[9].c_str());
            float g = std::atof(string_split[10].c_str());
            float b = std::atof(string_split[11].c_str());
            Color rgb(r, g, b);
            float diff_co = std::atof(string_split[5].c_str());

            auto sphere_tmp = Sphere::Sphere(center, radius, rgb, diff_co);
            s.push_back(sphere_tmp);
        }
        if (type == "dlight")
        {
            auto center = std::make_tuple(std::atof(string_split[1].c_str()),
                                 std::atof(string_split[2].c_str()),
                                 std::atof(string_split[3].c_str()));
            float r = std::atof(string_split[4].c_str());
            float g = std::atof(string_split[5].c_str());
            float b = std::atof(string_split[6].c_str());

            Color color(r, g, b);
            dir_light.push_back(Dlight::Dlight(center, color));
        }
        if (type == "alight")
        {
            float r = std::atof(string_split[1].c_str());
            float g = std::atof(string_split[2].c_str());
            float b = std::atof(string_split[3].c_str());
            alight = Color(r, g, b);
        }
    }
}


void apply(int i, int end, int height, int width,
           std::tuple<float, float, float> u,
           std::tuple<float, float, float> v,
           std::tuple<float, float, float> screen_center,
           Camera cam,
           std::vector<Sphere::Sphere> sphere_list,
           std::vector<Dlight::Dlight> dlight_list,
           Magick::PixelPacket *pix,
           Color alight)
{
    for (; i < end; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            auto out_width = Vector::add_vectors(screen_center,
                    Vector::scale(i - width / 2, v));
            auto out_height = Vector::add_vectors(screen_center,
                    Vector::scale(j - height / 2, u));
            auto eye = Vector::scale(-1, cam.get_center().to_tuple());
            auto outgoing_vector_tmp =
                Vector::add_vectors(Vector::add_vectors(out_width, out_height),
                        eye);
            auto tmp = Point(Vector::normalize(outgoing_vector_tmp));

            for (auto sphere_tmp : sphere_list)
            {
                auto intersect = sphere_tmp.intersect(cam.get_center(), tmp);
                if (intersect.get_x() != -1 && intersect.get_y() != -1
                        && intersect.get_z() != -1)
                {
                    Color color = Color();
                    for (auto dlight : dlight_list)
                    {
                        color = color + color_sphere_compute(sphere_tmp, dlight,
                                intersect, alight);
                    }
                    auto color_tuple = color.to_tuple();
                        Magick::ColorRGB rgb_color = Magick::ColorRGB(std::get<0>(color_tuple),
                                std::get<1>(color_tuple), std::get<2>(color_tuple));
                        pix[width * i + j] = rgb_color;
                }
            }
        }
    }
}


void seq_apply(int height, int width,
                    std::tuple<float, float, float> u,
                    std::tuple<float, float, float> v,
                    std::tuple<float, float, float> screen_center,
                    Camera cam,
                    std::vector<Sphere::Sphere> sphere_list,
                    std::vector<Dlight::Dlight> dlight_list,
                    Magick::PixelPacket *pix,
                    Color alight)

{

    double time;
    scoped_timer::scoped_timer *seq = new scoped_timer(time);
    apply(0, height, height, width, u, v, screen_center, cam, sphere_list, dlight_list,
    pix, alight);
    delete seq;
    std::cout << "Time elapsed seq: " << time << std::endl;
}


void parallel_apply(int threads, int height, int width,
                    std::vector<Sphere::Sphere> sphere_list,
                    std::vector<Dlight::Dlight> dlight_list,
                    Magick::PixelPacket *pix,
                    Color alight)

{
    // Examples for dev purposes, TODO: automatize the parsing of a config file
    // as input
    Camera::Camera cam(Point(1.0, 0.0, 0.0), Point(0.0, 1.0, 0.0),
            Point(0, 0, 0));

    // Begin the raytracing

    std::tuple<float, float, float> u = cam.u.to_tuple();
    std::tuple<float, float, float> v = cam.v.to_tuple();
    auto w = Vector::cross_product(u, v);

    float screen_center_eye_dist = (width/2) / tan(45.0/2);
    auto screen_center = Vector::add_vectors(cam.get_center().to_tuple(),
            Vector::scale(-screen_center_eye_dist, w));
    std::vector<std::thread *> pool;
    for (int i = 0; i < threads; ++i)
    {
        auto t = new std::thread(apply, (i * height / threads), (i + 1) *
        height / threads,
        height, width, u, v, screen_center, cam, sphere_list, dlight_list, pix,
        alight);
        pool.push_back(t);
    }

    double time;
    scoped_timer::scoped_timer *parallel = new scoped_timer(time);
    for (auto th : pool)
        th->join();
    delete parallel;
    std::string method;
    if (threads != 1)
        method = "parallel";
    else
        method = "sequantial";
    std::cout << "Time elapsed " << method << ": " << time << std::endl;
}

int main(int argc, char **argv)
{
    if (argc < 2)
        return -1;
    int height;
    int width;

    std::vector<Sphere::Sphere> sphere_list;
    std::vector<Dlight::Dlight> dlight_list;
    Color alight;

    conf_read(argv[1], height, width, sphere_list, dlight_list, alight);

    // Magick++ initialization
    Magick::Geometry canvas(width, height);
    Magick::Image img(canvas, Magick::Color("black"));
    Magick::PixelPacket *pix = img.setPixels(0, 0, width, height);

    parallel_apply(8, height, width, sphere_list,
    dlight_list, pix, alight);
    img.syncPixels();
    img.write("lol.png");
    return 0;
}
