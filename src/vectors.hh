#pragma once
#include <tuple>
#include <cmath>

namespace Vector
{
    template <typename T> T dot_product(std::tuple<T, T, T> a,
                                        std::tuple<T, T, T> b);
    template <typename T> std::tuple<T, T, T> scale(float coef, std::tuple<T, T, T> a);
    template <typename T> std::tuple<T, T, T> add_vectors(std::tuple<T, T, T> a, std::tuple<T, T, T> b);
    template <typename T> float distance(std::tuple<T, T, T> a, std::tuple<T, T, T> b);
    template <typename T> std::tuple<T, T, T> cross_product(std::tuple<T, T, T> a, std::tuple<T, T, T> b);
    template <typename T> T norm(std::tuple<T, T, T> a);
    template <typename T> std::tuple<T, T, T> normalize(std::tuple<T, T, T> a);
}

#include "vectors.hxx"
