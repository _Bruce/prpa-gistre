#pragma once
#include "point.hh"

/*
 * Entity is a parent class for all the kinds of shapes that we will encounter
 * (circle, spheres, etc.)
 */

class Entity
{
public:
    Entity(Point pos);
    ~Entity();
    virtual Point get_center() = 0;
protected:
    // Center position
    Point center;
};
