#pragma once

#include "vectors.hh"
#include <iostream>

namespace Vector
{
    template <typename T>
    T dot_product(std::tuple<T, T, T> a, std::tuple<T, T, T> b)
    {
        float a1 = std::get<0>(a);
        float b1 = std::get<0>(b);
        
        float a2 = std::get<1>(a);
        float b2 = std::get<1>(b);

        float a3 = std::get<2>(a);
        float b3 = std::get<2>(b);

        float x1 = a1 * b1;
        float x2 = a2 * b2;
        float x3 = a3 * b3;
        return x1 + x2 + x3;
    }

    template <typename T>
    std::tuple<T, T, T> scale(float coef, std::tuple<T, T, T> a)
    {
        return std::make_tuple(std::get<0>(a) * coef,
                               std::get<1>(a) * coef,
                               std::get<2>(a) * coef);
    }

    template <typename T>
    std::tuple<T, T, T> add_vectors(std::tuple<T, T, T> a,
                                            std::tuple<T, T, T> b) 
    {
        return std::make_tuple(std::get<0>(a) + std::get<0>(b),
                               std::get<1>(a) + std::get<1>(b),
                               std::get<2>(a) + std::get<2>(b));
    }

    template <typename T>
    float distance(std::tuple<T, T, T> a,
                           std::tuple<T, T, T> b)
    {
        auto tmp = add_vectors(a, scale(-1, b)); // A - B
        return sqrt(pow(std::get<0>(tmp), 2) + pow(std::get<1>(tmp), 2) +
                    pow(std::get<2>(tmp), 2));
    }

    template <typename T>
    std::tuple<T, T, T> cross_product(std::tuple<T, T, T> a, std::tuple<T, T, T> b)
    {
        T x = std::get<1>(a) * std::get<2>(b) - std::get<2>(a) * std::get<1>(b);
        T y = std::get<2>(a) * std::get<0>(b) - std::get<0>(a) * std::get<2>(b);
        T z = std::get<0>(a) * std::get<1>(b) - std::get<1>(a) * std::get<0>(b);
        return std::make_tuple(x, y, z);
    }

    template <typename T>
    T norm(std::tuple<T, T, T> a)
    {
        float x = std::get<0>(a);
        float y = std::get<1>(a);
        float z = std::get<2>(a);
        return sqrt(x*x + y*y + z*z);
    }

    template <typename T>
    std::tuple<T, T, T> normalize(std::tuple<T, T, T> a) 
    {
        auto norm = Vector::norm(a);
        auto x = std::get<0>(a);
        auto y = std::get<1>(a);
        auto z = std::get<2>(a);
        return std::make_tuple(x/norm, y/norm, z/norm); } }
