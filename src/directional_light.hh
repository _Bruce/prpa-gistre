#pragma once
#include "color.hh"
#include <tuple>

/*
 * Represents a directional light (a light that is pointed towards a specific
 * direction
 */

class Dlight
{
public:
    Dlight(std::tuple<float, float, float> dir, Color color);
    ~Dlight();
    Dlight();

    std::tuple<float, float, float> get_dir();
    Color get_color();
private:
    std::tuple<float, float, float> dir;
    Color color;
};
