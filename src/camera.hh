#pragma once
#include "entity.hh"
#include "vectors.hh"
#include "point.hh"

/*
 * Camera represents the « eyes » of the scene (id: the origin of light
 * rays)
 */

class Camera : public Entity
{
public:
    Camera(Point u1, Point v1, Point center);
    ~Camera();

    Point get_center() override;
    // Coordinates for the orthogonal vectors (x, y, z)
    const Point u;
    const Point v;
};
