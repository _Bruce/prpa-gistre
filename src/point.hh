#pragma once
#include <tuple>

class Point
{
public:
    Point(float x, float y, float z);
    Point(std::tuple<float, float, float> coor);
    ~Point();
    std::tuple<float, float, float> to_tuple() const;
    float get_x();
    float get_y();
    float get_z();
private:
    const float x;
    const float y;
    const float z;
};
