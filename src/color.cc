#include "color.hh"

Color::Color(float r, float g, float b)
{
    this->r = (r <= 1.0f && r >= 0.0f) ? r : r/255.0f;
    this->g = (g <= 1.0f && g >= 0.0f) ? g : g/255.0f;
    this->b = (b <= 1.0f && b >= 0.0f) ? b : b/255.0f;
}

Color::Color()
{
    this->r = 0.0f; 
    this->g = 0.0f; 
    this->b = 0.0f;
}

Color::~Color()
{}

float Color::get_r()
{
    return this->r;
}

float Color::get_g()
{
    return this->g;
}

float Color::get_b()
{
    return this->b;
}

Color Color::operator*(float rhs)
{
    float nr = this->r * rhs;
    float ng = this->g * rhs;
    float nb = this->b * rhs;

    nr = (nr <= 1.0) ? nr : 1.0;
    ng = (ng <= 1.0) ? ng : 1.0;
    nb = (nb <= 1.0) ? nb : 1.0;

    return Color(nr, ng, nb);
}

Color Color::operator*(Color rhs)
{
    float nr = this->r * rhs.get_r();
    float ng = this->g * rhs.get_g();
    float nb = this->b * rhs.get_b();
    return Color(nr, ng, nb);
}

Color Color::operator+(Color rhs)
{
    float nr = this->r + rhs.get_r();
    float ng = this->g + rhs.get_g();
    float nb = this->b + rhs.get_b();

    if (nr < 0.0 || nr > 1.0)
        nr = (nr < 0.0) ? 0.0 : 1.0;

    if (ng < 0.0 || ng > 1.0)
        ng = (ng < 0.0) ? 0.0 : 1.0;

    if (nb < 0.0 || nb > 1.0)
        nb = (nb < 0.0) ? 0.0 : 1.0;

    return Color(nr, ng, nb);
}

std::tuple<float, float, float> Color::to_tuple()
{
    return std::make_tuple(this->r, this->g, this->b);
}
